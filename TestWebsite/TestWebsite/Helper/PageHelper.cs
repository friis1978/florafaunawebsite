﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace TestWebsite.Helper
{
    public class PageHelper
    {
        public static List<SelectListItem> CreatePageSizeList(int val)
        {
            List<SelectListItem> items = new List<SelectListItem>();
            int[] values = { 5, 10, 25, 50, 100, 250, 500 };
            foreach (var v in values)
                items.Add(new SelectListItem{ Text = v.ToString(), Value = v.ToString() , Selected = (v==val) });
            return items;
        }
    }
}