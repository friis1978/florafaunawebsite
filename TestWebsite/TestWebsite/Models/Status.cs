//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace TestWebsite.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Status
    {
        public Status()
        {
            this.Art = new HashSet<Art>();
        }
    
        public int status_id { get; set; }
        public string beskrivelse { get; set; }
    
        public virtual ICollection<Art> Art { get; set; }
    }
}
