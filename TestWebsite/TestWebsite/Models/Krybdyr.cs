//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace TestWebsite.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Krybdyr
    {
        public Krybdyr()
        {
            this.Art = new HashSet<Art>();
        }
    
        public int Krybdyr_Id { get; set; }
        public Nullable<int> Levested { get; set; }
    
        public virtual ICollection<Art> Art { get; set; }
        public virtual e_Levested e_Levested { get; set; }
    }
}
