//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace TestWebsite.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class e_Levested
    {
        public e_Levested()
        {
            this.Krybdyr = new HashSet<Krybdyr>();
        }
    
        public int Levested_id { get; set; }
        public string Levested_navn { get; set; }
    
        public virtual ICollection<Krybdyr> Krybdyr { get; set; }
    }
}
