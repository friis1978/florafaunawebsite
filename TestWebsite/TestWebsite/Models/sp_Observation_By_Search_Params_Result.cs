using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace TestWebsite.Models
{
    using System;
    
    public partial class sp_Observation_By_Search_Params_Result
    {
        [Key]
        public int Observations_id { get; set; }
        public string Observationsnote { get; set; }
        public int Bruger_Id { get; set; }
        public string Artnavn { get; set; }
        public string Art_beskrivelse { get; set; }
        public string Familie { get; set; }
        public string Orden { get; set; }
        public string Klasse { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public Nullable<System.DateTime> ObservationsDag { get; set; }
        public string Bynavn { get; set; }
        public string GEO { get; set; }
    }
}
