//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace TestWebsite.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class vw_GuldeSmede
    {
        public int Art_Id { get; set; }
        public int Organisme_Id { get; set; }
        public string OrganismeNavn { get; set; }
        public int Familie_Id { get; set; }
        public string Navn { get; set; }
        public string Beskrivelse { get; set; }
        public int status_id { get; set; }
        public Nullable<int> Guldsmede_Id { get; set; }
        public string InsektFødeType { get; set; }
    }
}
