using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace TestWebsite.Models
{
    public class UserObservationModel
    {
        [Key]
        public int Obs_Id { get; set; }
        [Required]
        [Display(Name = "Observations Note")]
        public string Obs_Note { get; set; }
        [Required]
        [Display(Name = "Art Navn")]
        public string Art_Navn { get; set; }
        [Required]
        [Display(Name = "Art Beskrivelse")]
        public string Art_Beskrivelse { get; set; }
        [Required]
        [Display(Name = "Art Familie")]
        public string Art_Familie { get; set; }
        [Required]
        [Display(Name = "Art Orden")]
        public string Art_Orden { get; set; }
        [Required]
        [Display(Name = "Art Klasse")]
        public string Art_Klasse { get; set; }
        [Required]
        [Display(Name = "By")]
        public string Bynavn { get; set; }
        [Required]
        [Display(Name = "Lokation")]
        public string Geo_lokation { get; set; }       
    }
}