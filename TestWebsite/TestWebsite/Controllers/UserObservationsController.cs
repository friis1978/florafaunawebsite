﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using TestWebsite.Models;
using Microsoft.AspNet.Identity;
using PagedList;
using TestWebsite.Helper;
using System.Diagnostics;

namespace TestWebsite.Controllers
{
    public class UserObservationsController : Controller
    {
        [Authorize]
        public ActionResult Index(string sortOrder, string currentFilter, string searchString, int? page, int? pageSize)
        {           
            FloraFaunaWebsite_db_Entities1 observation_db = new FloraFaunaWebsite_db_Entities1();

            int UserId = User.Identity.GetUserId<int>();

            ViewBag.CurrentSort = sortOrder;

            // Adding search parameters for sorting the list
            ViewBag.DatoSortParm = sortOrder == "dato" ? "dato_desc" : "dato";
            ViewBag.BySortParm = sortOrder == "by" ? "by_desc" : "by";
            ViewBag.LokationSortParm = sortOrder == "lokation" ? "lokation_desc" : "lokation";
            ViewBag.NavnSortParm = sortOrder == "navn" ? "navn_desc" : "navn";
            ViewBag.KlasseSortParm = sortOrder == "klasse" ? "klasse_desc" : "klasse";
            ViewBag.FamilieSortParm = sortOrder == "familie" ? "familie_desc" : "familie";
            ViewBag.OrdenSortParm = sortOrder == "orden" ? "orden_desc" : "orden";

            if (searchString != null)
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }

            ViewBag.CurrentFilter = searchString;

            var observations = observation_db.GetObservationsByParams(null, null, null, "", null, 1, null,null);

            // add filtering functionality to the index
            if (!String.IsNullOrEmpty(searchString))
            {
                sortOrder = "search";                
            }

            Debug.WriteLine("sortorder: " + sortOrder);
                                                                                                      
            // sort the observations
            switch (sortOrder)
            {
                case "search":
                    observations = observation_db.GetObservationsByParams(null, null, null, searchString, null, UserId, null,null); 
                    break;
                case "dato":
                    observations = observation_db.GetObservationsByParams(null, null, null,"", null, UserId, null, "Dato");
                    break;             
                case "by":
                    observations = observation_db.GetObservationsByParams(null, null, null,"", null, UserId, null, "By");
                    break;              
                case "lokation":
                    observations = observation_db.GetObservationsByParams(null, null, null,"", null, UserId, null, "Lokation");
                    break;                
                case "navn":
                    observations = observation_db.GetObservationsByParams(null, null, null,"", null, UserId, null, "ArtNavn");
                    break;               
                case "klasse":
                    observations = observation_db.GetObservationsByParams(null, null, null,"", null, UserId, null, "ArtKlasse");
                    break;            
                case "familie":
                    observations = observation_db.GetObservationsByParams(null, null, null,"", null, UserId, null, "ArtFamilie");
                    break;             
                case "orden":
                    observations = observation_db.GetObservationsByParams(null, null, null,"", null, UserId, null, "ArtOrden");
                    break;              
                default:                           
                    observations = observation_db.GetObservationsByParams(null, null, null,"", null, UserId, null, "By");
                    break;
            }

            var new_obs = observations.ToList();
            int ps = (pageSize ?? 10);
            ViewBag.PageSize = PageHelper.CreatePageSizeList(ps);
            ViewBag.CurrentPageSize = ps;
            int pageNumber = (page ?? 1);
            return View(new_obs.ToPagedList(pageNumber, ps));
        }

        public ActionResult AllObservations(string sortOrder, string currentFilter, string searchString, int? page, int? pageSize)
        {
            FloraFaunaWebsite_db_Entities1 observation_db = new FloraFaunaWebsite_db_Entities1();
           
            ViewBag.CurrentSort = sortOrder;

            // Adding search parameters for sorting the list
            ViewBag.DatoSortParm = sortOrder == "dato" ? "dato_desc" : "dato";
            ViewBag.BySortParm = sortOrder == "by" ? "by_desc" : "by";
            ViewBag.LokationSortParm = sortOrder == "lokation" ? "lokation_desc" : "lokation";
            ViewBag.NavnSortParm = sortOrder == "navn" ? "navn_desc" : "navn";
            ViewBag.KlasseSortParm = sortOrder == "klasse" ? "klasse_desc" : "klasse";
            ViewBag.FamilieSortParm = sortOrder == "familie" ? "familie_desc" : "familie";
            ViewBag.OrdenSortParm = sortOrder == "orden" ? "orden_desc" : "orden";

            if (searchString != null)
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }

            ViewBag.CurrentFilter = searchString;

            var observations = observation_db.GetObservationsByParams(null, null, null, "", null,null, null, null);

            // add filtering functionality to the index
            if (!String.IsNullOrEmpty(searchString))
            {
                sortOrder = "search";
            }

            Debug.WriteLine("sortorder: " + sortOrder);

            // sort the observations
            switch (sortOrder)
            {
                case "search":
                    observations = observation_db.GetObservationsByParams(null, null, null, searchString, null,null, null, null);
                    break;
                case "dato":
                    observations = observation_db.GetObservationsByParams(null, null, null, "", null,null, null, "Dato");
                    break;
                case "by":
                    observations = observation_db.GetObservationsByParams(null, null, null, "", null,null, null, "By");
                    break;
                case "lokation":
                    observations = observation_db.GetObservationsByParams(null, null, null, "", null,null, null, "Lokation");
                    break;
                case "navn":
                    observations = observation_db.GetObservationsByParams(null, null, null, "", null,null, null, "ArtNavn");
                    break;
                case "klasse":
                    observations = observation_db.GetObservationsByParams(null, null, null, "", null,null, null, "ArtKlasse");
                    break;
                case "familie":
                    observations = observation_db.GetObservationsByParams(null, null, null, "", null,null, null, "ArtFamilie");
                    break;
                case "orden":
                    observations = observation_db.GetObservationsByParams(null, null, null, "", null,null, null, "ArtOrden");
                    break;
                default:
                    observations = observation_db.GetObservationsByParams(null, null, null, "", null,null, null, "By");
                    break;
            }

            var new_obs = observations.ToList();
            int ps = (pageSize ?? 10);
            ViewBag.PageSize = PageHelper.CreatePageSizeList(ps);
            ViewBag.CurrentPageSize = ps;
            int pageNumber = (page ?? 1);
            return View(new_obs.ToPagedList(pageNumber, ps));
        }

    }
}
