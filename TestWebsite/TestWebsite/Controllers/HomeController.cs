﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;

namespace TestWebsite.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }
                   
        [Authorize]
        public ActionResult Contact()
        {
            bool admin = User.IsInRole("Admin");
            bool guldmedlem = User.IsInRole("Gold User");
            int UserId = User.Identity.GetUserId<int>();
 
            if(admin)
            {
                ViewBag.Message = "Du er admin med id: "+UserId;
            }
            else
            {
                if(guldmedlem)
                {
                    ViewBag.Message = "Du er guldmedlem med id: "+UserId;
                }
                else
                { 
                    ViewBag.Message = "Du er almindelig bruger med id: "+UserId; 
                }                          
            }
           
            return View();
        }

        [Authorize(Roles = "Admin")]
        public ActionResult AdminPage()
        {                       
            ViewBag.Title = "Adminstrator siden";
            ViewBag.Message = "Velkommen" + User.Identity.GetUserName();
            return View();
        }
    }
}