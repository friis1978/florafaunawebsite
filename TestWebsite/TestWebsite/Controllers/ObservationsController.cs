﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using TestWebsite.Models;
using Microsoft.AspNet.Identity;
using System.Data.SqlClient;

namespace TestWebsite.Controllers
{
    public class ObservationsController : Controller
    {
        private FloraFaunaWebsite_db_Entities1 db = new FloraFaunaWebsite_db_Entities1();
        

        // GET: Observations
        public ActionResult Index()
        { 
            int UserId = User.Identity.GetUserId<int>();
            
            try
            {
                var observations = db.GetObservationsByParams(null,null,null,"",null,UserId,null,"Dato");
                return View(observations.ToList());
            }
            catch (Exception ex)
            {
                ViewBag.errorMessage = ex.Message.ToString();
                return View();
            }                                                                           
        }

        // GET: Observations/Details/5
        public ActionResult Details(int? id)
        {
            int UserId = User.Identity.GetUserId<int>();
            var observations = db.GetObservationsByParams(null, null, null,"",UserId, null, null,"Dato"); 

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var observation = observations.Where(i => i.Observations_id == id);
            
            if (observation == null)
            {
                return HttpNotFound();
            }
            return View(observation);
        }

        // GET: Observations/Create
        public ActionResult Create()
        {
            ViewBag.Art_Id = new SelectList(db.Art, "Art_Id", "Navn");
            return View();
        }

        // POST: Observations/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Observations_Id,Art_Id,Bruger_Id,Lokation_Zip,Lokation_Amt,Lokation_Kommune,Lokation_Geo,Observationsnote")] Observation observation)
        {
            if (ModelState.IsValid)
            {
                db.Observation.Add(observation);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.Art_Id = new SelectList(db.Art, "Art_Id", "Navn", observation.Art_Id);
            return View(observation);
        }

        // GET: Observations/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Observation observation = db.Observation.Find(id);
            if (observation == null)
            {
                return HttpNotFound();
            }
            ViewBag.Art_Id = new SelectList(db.Art, "Art_Id", "Navn", observation.Art_Id);
            return View(observation);
        }

        // POST: Observations/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Observations_Id,Art_Id,Bruger_Id,Lokation_Zip,Lokation_Amt,Lokation_Kommune,Lokation_Geo,Observationsnote")] Observation observation)
        {
            if (ModelState.IsValid)
            {
                db.Entry(observation).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.Art_Id = new SelectList(db.Art, "Art_Id", "Navn", observation.Art_Id);
            return View(observation);
        }

        // GET: Observations/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Observation observation = db.Observation.Find(id);
            if (observation == null)
            {
                return HttpNotFound();
            }
            return View(observation);
        }

        // POST: Observations/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Observation observation = db.Observation.Find(id);
            db.Observation.Remove(observation);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
